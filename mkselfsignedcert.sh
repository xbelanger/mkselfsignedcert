#!/bin/bash

# Xavier Belanger
# July 27, 2024

# this script is covered by the BSD 2-Clause License

# generate a self-signed certificate, valid for six months by default (180 days)

# cheking if an argument is present
if [ $# -lt 1 ]
then
	/bin/printf "usage : $0 CommonName (FQDN)\n"
	exit 1
fi

# using the script argument as the full hostname, in lower case
fullHostname=$(echo $1 | /usr/bin/tr [:upper:] [:lower:])

# checking if the request is for a wildcard certificate
echo $fullHostname | /bin/grep -q "^*"
if [ $? -eq 0 ]
then
	/usr/bin/printf "This request will generate a wildcard certificate\n"

	while [ "$confirmation" != "yes" ]
	do
		read -p "Please confirm [Y/N] " -n 1 -r response
		/usr/bin/printf "\n"

		case "$response" in
			[yY])
				confirmation="yes"
				CommonName=$fullHostname
			;;
			[nN])
				exit 1
			;;
			*)
				/usr/bin/printf "Please respond with yes or no (y/n)."
			;;
		esac
	done
else
	# checks if the argument is a valid server FQDN (see RFC 952 and RFC 1123)
	#  - hostname should contain [0-9][a-zA-Z] dash (-) and dot (.) only
	#  - does not start or ends with a dash or a dot
	#  - 2 characters minimum, 63 characters maximum
	if ! [ $(echo $fullHostname | /bin/grep '^[a-zA-Z0-9][-.a-zA-Z0-9]\{0,61\}[a-zA-Z0-9]$') ]
	then
		/bin/printf "Error: server-hostname should contain [0-9],[a-zA-Z], dash (-) and dot (.) only,\n"
		/bin/printf "       and should not start or ends with a dash or a dot, 2 characters minimum\n"
		/bin/printf "       63 characters maximum.\n"
		exit 2
	else
		CommonName=$fullHostname
	fi
fi

if [ "$confirmation" == "yes" ]
then
	wildcard=$(echo $CommonName | /usr/bin/sed 's/*/wildcard/')
	filename=$wildcard"-"$(/bin/date +"%F")
else
	filename=$CommonName-$(/bin/date +"%F")
fi

subject="/C=XX/ST=STATE/L=CITY/CN=$CommonName"

# minimum file permissions
umask 077

# validity (number of days)
days=180

# certficate request generation
/usr/bin/openssl req -new -nodes -keyout $filename.key -out $filename.csr -subj "$subject"

# certficate generation
# options varies based on the OpenSSL version
/usr/bin/openssl version | /bin/grep -q "SSL 3"

if [ $? -eq 0 ]
then
	/usr/bin/openssl req -x509 -sha256 -days $days -copy_extensions none -key $filename.key -in $filename.csr -out $filename.pem
else
	/usr/bin/openssl req -x509 -sha256 -days $days -key $filename.key -in $filename.csr -out $filename.pem
fi

# adding read permissions to the public file
/bin/chmod 644 $filename.pem

# certificate review
/usr/bin/openssl x509 -in $filename.pem -text -noout

# EoF
