# mkselfsignedcert

This script generates a self-signed PEM certificate, valid for six months by default (180 days).

## Usage

Before using this script you should adjust the "subject" variable (line 68) to set the proper country code, state and city based on where your server or organization is located.

You must provide the full qualified domain name (FQDN) for the server that will be using the certificate:

```
$ ./mkselfsignedcert server.example.net
```

Three files will be created:

 - a .key file for the private key
 - a .csr file for the certificate request
 - a .pem file for the certificate

All file names will include a timestamp (*server.example.net-2019-08-31.pem* for instance).

All files will have permissions set to a minimum for their use.

